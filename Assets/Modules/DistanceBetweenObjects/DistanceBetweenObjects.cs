﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceBetweenObjects : MonoBehaviour
{

    public GameObject Notausgang1, Notausgang2, Notausgang3;
    public LineRenderer myLine;
    public DistanceInteraction distInteraction;

    public float distToAusgang1;
    public float distToAusgang2;
    public float distToAusgang3;

    //public GameObject distanceText;
    public Text distanceText;


    // Start is called before the first frame update
    void Start()
    {
        myLine = this.GetComponent<LineRenderer>();
        myLine.enabled = false;

        distInteraction = GameObject.FindObjectOfType<DistanceInteraction>();
        distanceText.gameObject.GetComponent<Text>().enabled = false;

    }


    void Awake()
    {
        Notausgang1 = GameObject.Find("Notausgang1");
        Notausgang2 = GameObject.Find("Notausgang2");
        Notausgang3 = GameObject.Find("Notausgang3");

        distanceText = GameObject.FindObjectOfType<Text>();
        distanceText.gameObject.GetComponent<Text>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (distInteraction.distanceMove == false)
        {
            myLine.enabled = false;
            distanceText.gameObject.GetComponent<Text>().enabled = false;
        }
            


        if (distInteraction.distanceMove == true)
        {
            if (this.CompareTag("Selectable"))
            {
                distToAusgang1 = Vector3.Distance(this.transform.position, Notausgang1.transform.position);
                distToAusgang2 = Vector3.Distance(this.transform.position, Notausgang2.transform.position);
                distToAusgang3 = Vector3.Distance(this.transform.position, Notausgang3.transform.position);

                myLine.enabled = true;

                // defining the shortest distance from this gameobject to one of the exit points
                if (distToAusgang2 > distToAusgang1 && distToAusgang3 > distToAusgang1)
                {
                    myLine.SetPosition(0, this.transform.position);
                    myLine.SetPosition(1, Notausgang1.transform.position);

                    distanceText.gameObject.GetComponent<Text>().enabled = true;
                    distanceText.text = "Notausgang in " + distToAusgang1.ToString("F2") + "m";
                }

                if (distToAusgang1 > distToAusgang2 && distToAusgang3 > distToAusgang2)
                {
                    myLine.SetPosition(0, this.transform.position);
                    myLine.SetPosition(1, Notausgang2.transform.position);

                    distanceText.gameObject.GetComponent<Text>().enabled = true;
                    distanceText.text = "Notausgang in " + distToAusgang2.ToString("F2") + "m";
                }

                if (distToAusgang1 > distToAusgang3 && distToAusgang2 > distToAusgang3)
                {
                    myLine.SetPosition(0, this.transform.position);
                    myLine.SetPosition(1, Notausgang3.transform.position);

                    distanceText.gameObject.GetComponent<Text>().enabled = true;
                    distanceText.text = "Notausgang in " + distToAusgang3.ToString("F2") + "m";
                }
            }
        }
            


            

                

            
        
        
    }
}
