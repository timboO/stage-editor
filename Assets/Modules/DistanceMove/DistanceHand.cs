﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceHand : MonoBehaviour
{
    public Transform _selection;

    [Header("Objects Layer")]
    [SerializeField] private string selectableTag = "Selectable";
    public LayerMask layerMask;

    [Header("Object")]    
    public float rotationSpeed = 5f;
    public int rotThreshold = 45;
    private Vector2 thumbstickValue;
    public GameObject lastTarget;

    [Header("ChangeColor")]
    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Material defaultMaterial;
    public GameObject uiInteraction;
    Transform instancesParent;
    public TagManager tagManager;
    public bool tintMode = false;
    public bool menuActive;
    public bool ausActive;

    [Header("LineRenderer")]
    public LineRenderer myLine;
    public int rayLength = 50;
    public GameObject indexTip;
    public Material targetMat;

    [Header("Scale")]
    public GameObject LeftHand;
    public float scaleMult = 1f;

    [Header("Debug")]
    public bool selectRay = false;
    public bool distanceMove = false;
    public bool highlighted;
    public bool leftTriggerTrue = false;


    // Update is called once per frame
    void Update()
    {
        selectRay = false;
        myLine.enabled = false;
        distanceMove = false;

        ausActive = uiInteraction.GetComponent<MenuActive>().ausActive;
        menuActive = uiInteraction.GetComponent<MenuActive>().menuActive;

        
            // while menu is inactive, distance interaction is active
            if (menuActive == false)
            {
                // targetMat to be tinted by color selection
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    myLine.material = targetMat;
                    tagManager.GetAllSelectables();
                }

                if (OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    selectRay = true;
                    myLine.enabled = true;

                    if (_selection != null)
                    {
                        var selectionRenderer = _selection.GetComponent<Renderer>();
                        selectionRenderer.material = defaultMaterial;
                        _selection = null;
                    }
                    else
                    {
                        highlighted = false;
                    }

                    Vector3 forward = this.transform.forward * rayLength;

                    //draw LineRenderer between pos0 and pos1
                    myLine.SetPosition(0, indexTip.transform.position);
                    myLine.SetPosition(1, forward);

                    //on ray hit event
                    RaycastHit hit;
                    if (Physics.Raycast(indexTip.transform.position, forward, out hit, layerMask))
                    {
                        var selection = hit.transform;
                        if (selection.CompareTag(selectableTag))
                        {
                            var selectionRenderer = selection.GetComponent<Renderer>();
                            if (selectionRenderer != null)
                            {
                                highlighted = true;
                                selectionRenderer.material = highlightMaterial;
                            }

                            _selection = selection;
                            lastTarget = _selection.gameObject;

                            // transform the gameobject
                            // Making the object follow my hand based on how much you press the handTrigger
                            if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) > 0.5f)
                            {
                                if (_selection != null)
                                {
                                    distanceMove = true;
                                    _selection.transform.SetParent(this.transform);

                                    //left Thumbstick Values
                                    thumbstickValue = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch) * rotationSpeed;                                                                     

                                    if (leftTriggerTrue == false)
                                    {   //handrotation = objects rotation                        
                                        _selection.transform.rotation = LeftHand.transform.rotation;

                                        //dynamic rotation
                                        //_selection.transform.Rotate(LeftHand.transform.rotation.x * rotationSpeed, LeftHand.transform.rotation.y * rotationSpeed, LeftHand.transform.rotation.z * rotationSpeed);
                                    }

                                //keep the tag selectable only on the now selected object (NOTE: runs in update as long as trigger is > 0.5)
                                tagManager.SetSelectableActive(_selection.transform);
                                }
                            }                            

                        }
                        
                        // when letting loose, unparent it again
                        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) < 0.1f)
                        {
                            distanceMove = false;
                            if (_selection != null)
                                _selection.transform.SetParent(instancesParent);
                            else
                                tagManager.ResetAllToSelectable();
                        }


                    }
                    else //Debug.Log("Hit nothing)");
                    {
                        highlighted = false;
                        if (lastTarget != null)
                            lastTarget.transform.SetParent(instancesParent);
                        else
                            tagManager.ResetAllToSelectable();
                    }
                }
            }
        
    }
}
