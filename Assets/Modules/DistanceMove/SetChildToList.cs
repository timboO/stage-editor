﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetChildToList : MonoBehaviour
{
    public TagManager tagManager;
    public GameObject lastTarget;
    public Transform curSelection;
    private int childCount = 0;


    // Update is called once per frame
    void Update()
    {
        lastTarget = this.GetComponent<DistanceInteraction>().lastTarget;
        curSelection = this.GetComponent<DistanceInteraction>()._selection;
        

        if(lastTarget != null)
        {
            if (curSelection == null)
            {
                // has this GO a child
                // if childcount is 0 --> there is no child
                childCount = this.gameObject.transform.childCount;

                if (childCount != 0)
                {
                    this.gameObject.transform.GetChild(0).SetParent(tagManager.transform);
                    //lastTarget.transform.SetParent(tagManager.transform);
                }
            }
        }
    }
}
