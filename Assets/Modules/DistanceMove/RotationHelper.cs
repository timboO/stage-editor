﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHelper : MonoBehaviour
{

    public StickyObjects stickyObjects;
    private bool isDistGrabbed;

    // Start is called before the first frame update
    void Awake()
    {
        stickyObjects = this.GetComponentInParent<StickyObjects>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isDistGrabbed == true){
            this.GetComponent<Collider>().enabled = true;
        }else{
            this.GetComponent<Collider>().enabled = false;
        }
    }
}
