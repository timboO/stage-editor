﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceInteraction : MonoBehaviour
{
    public Transform _selection;

    [Header("Objects Layer")]
    [SerializeField] private string selectableTag = "Selectable";
    public LayerMask layerMask, deletemask;

    [Header("Object")]
    public float rotationSpeed = 5f;
    private Vector2 thumbstickValue;
    public GameObject lastTarget;
    public float waitDur = 0.5f;
    public float distSpeedMult = 0.5f;
    
    [Header("ChangeColor")]
    [SerializeField] public Material tintMaterial;
    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Material defaultMaterial;
    public GameObject uiInteraction;
    Transform instancesParent;
    public TagManager tagManager;
    public bool tintMode = false;
    public bool menuActive;
    public bool ausActive;

    [Header("LineRenderer")]
    public LineRenderer myLine;
    public int rayLength = 25;
    public GameObject rightHand, indexTip;
    public Material targetMat, eraseMat;

    [Header("Scale")]
    public GameObject RightHand;
    public GameObject LeftHand;
    public float handDistance;
    public float scaleMult = 1f;

    [Header("Debug")]
    public bool selectRay = false;
    public bool deleteRay = false;
    public bool distanceMove = false;
    public bool highlighted;
    public bool leftIndexTrue = false;
    public bool leftHandTrue = false;
    private BigSmall bigSmall;


    void Start()
    {
        lastTarget = GameObject.Find("LastTarget_Dummy"); 
        instancesParent = uiInteraction.GetComponent<MenuActive>().instancesParent;

        bigSmall = GameObject.FindObjectOfType<BigSmall>();
    }

    // Update is called once per frame
    void Update()
    {
        selectRay = false;
        myLine.enabled = false;
        distanceMove = false;

        tintMode = uiInteraction.GetComponent<MenuActive>().ausActive;
        ausActive = uiInteraction.GetComponent<MenuActive>().ausActive;
        menuActive = uiInteraction.GetComponent<MenuActive>().menuActive;

        if (bigSmall.playerIsBig == false)
        {
            // while menu is inactive, distance interaction is active
            if (menuActive == false)
            {
                // targetMat to be tinted by color selection
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    myLine.material = targetMat;
                    tagManager.GetAllSelectables();
                }

                if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
                {
                    myLine.material = eraseMat;
                }


                if (OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    selectRay = true;
                    myLine.enabled = true;

                    if (_selection != null)
                    {
                        var selectionRenderer = _selection.GetComponent<Renderer>();
                        selectionRenderer.material = defaultMaterial;
                        _selection = null;
                    }
                    else
                    {
                        highlighted = false;
                    }

                    Vector3 forward = this.transform.forward * rayLength;

                    //draw LineRenderer between pos0 and pos1
                    myLine.SetPosition(0, indexTip.transform.position);
                    myLine.SetPosition(1, forward);

                    //on ray hit event
                    RaycastHit hit;
                    if (Physics.Raycast(indexTip.transform.position, forward, out hit, layerMask))
                    {
                        var selection = hit.transform;
                        if (selection.CompareTag(selectableTag))
                        {
                            var selectionRenderer = selection.GetComponent<Renderer>();
                            if (selectionRenderer != null)
                            {
                                highlighted = true;
                                selectionRenderer.material = highlightMaterial;
                            }

                            _selection = selection;
                            lastTarget = _selection.gameObject;

                            // transform the gameobject
                            // Making the object follow my hand based on how much you press the handTrigger
                            if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) > 0.5f)
                            {
                                if (_selection != null)
                                {
                                    distanceMove = true;
                                    _selection.transform.SetParent(this.transform);

                                    //left Thumbstick Values
                                    thumbstickValue = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch) * rotationSpeed;

                                    // Add Distance along Ray
                                    if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch) > 0.5 && leftHandTrue == false)
                                    {
                                        leftIndexTrue = true;
                                        float distSpeed = thumbstickValue.y * distSpeedMult;
                                        //selected objects distance = thumbstick value along direction over speed
                                        _selection.transform.position += forward * distSpeed * Time.deltaTime;
                                    }
                                    else
                                    {
                                        leftIndexTrue = false;
                                    }

                                    // Scale Selected Object
                                    if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch) > 0.5f && leftIndexTrue == false)
                                    {
                                        leftHandTrue = true;
                                        _selection.transform.SetParent(instancesParent);

                                        //distance between both hands as translation for objects localscale
                                        //handDistance = Vector3.Distance(LeftHand.transform.position, RightHand.transform.position) * scaleMult;
                                        //_selection.transform.localScale = new Vector3(handDistance, handDistance, handDistance);

                                        //thumbstick.y interaction instead of handDistance
                                        if (thumbstickValue.y != 0)
                                        {
                                            scaleMult = thumbstickValue.y * Time.deltaTime;
                                            _selection.transform.localScale = new Vector3(_selection.transform.localScale.x + scaleMult, _selection.transform.localScale.y + scaleMult, _selection.transform.localScale.z + scaleMult);

                                            //limiting scale values
                                            if (_selection.transform.localScale.y > 4f)
                                                _selection.transform.localScale = new Vector3(4, 4, 4);

                                            if (_selection.transform.localScale.y < 0.1f)
                                                _selection.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                                        }
                                        
                                    }
                                    else
                                    {
                                        leftHandTrue = false;
                                    }


                                    // Rotate Selected Object, when not setting distance
                                    // Rotation limited when near ground
                                    if (leftIndexTrue == false && leftHandTrue == false)
                                    {
                                        if (_selection.GetComponent<StickyObjects>().isGroundArea == true)
                                        {
                                            // rotate Y axis, when near ground
                                            //_selection.transform.Rotate(0, -thumbstickValue.x, 0, Space.World);
                                            _selection.transform.Rotate(0, -thumbstickValue.x, 0, Space.Self);
                                        }
                                        else
                                        {
                                            // rotate over left thumbstick
                                            //_selection.transform.Rotate(thumbstickValue.y, -thumbstickValue.x, 0, Space.World);
                                            _selection.transform.Rotate(-thumbstickValue.y, -thumbstickValue.x, 0, Space.Self);

                                        }
                                    }

                                    //keep the tag selectable only on the now selected object (NOTE: runs in update as long as trigger is > 0.5)
                                    tagManager.SetSelectableActive(_selection.transform);                                   
                                }
                            }
                        }                        

                        // when letting loose, unparent it again
                        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) < 0.1f)
                        {
                            distanceMove = false;
                            if (_selection != null)
                                _selection.transform.SetParent(instancesParent);
                            else
                                tagManager.ResetAllToSelectable();
                        }
                    }
                    else //Debug.Log("Hit nothing)");
                    {
                        highlighted = false;
                        if (lastTarget != null)
                            lastTarget.transform.SetParent(instancesParent);
                        else
                            tagManager.ResetAllToSelectable();
                    }
                }
            }

            // when button is not pressed any longer, unparent and reset
            if (OVRInput.GetUp(OVRInput.Button.One, OVRInput.Controller.RTouch))
            {
                highlighted = false;

                if (_selection != null)
                {
                    var selectionRenderer = _selection.GetComponent<Renderer>();
                    selectionRenderer.material = defaultMaterial;

                    _selection.transform.parent = instancesParent;
                    _selection = null;
                }
            }


            // Destroy GameObject on Erase Raycast Hit
            if (OVRInput.Get(OVRInput.Button.Two, OVRInput.Controller.RTouch))
            {
                myLine.enabled = true;
                deleteRay = true;

                Vector3 forward = this.transform.forward * rayLength;

                myLine.SetPosition(0, indexTip.transform.position);
                myLine.SetPosition(1, forward);

                tagManager.GetAllSelectables(); // common OVR Grabbable resets GO to highest hierarchy, we need to grab them again

                RaycastHit hit;
                if (Physics.Raycast(indexTip.transform.position, forward, out hit, deletemask))
                {                    
                    var selection = hit.transform;
                    if (selection.CompareTag(selectableTag) || selection.CompareTag("NotSelectable"))
                    {
                        tagManager.RemoveSelectable(selection.gameObject.transform);
                        Destroy(selection.gameObject);
                    }
                }
            }
            else
            {
                deleteRay = false;
            }


            // if ausActive is True, while in Menu, do the target stuff to tint objects;
            if ((ausActive == true) && (menuActive == true))
            {
                // targetMat to be tinted by color selection
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    myLine.material = targetMat;
                }

                if (OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch))
                {
                    selectRay = true;
                    myLine.enabled = true;

                    if (_selection != null)
                    {
                        var selectionRenderer = _selection.GetComponent<Renderer>();
                        selectionRenderer.material = defaultMaterial;
                        _selection = null;
                    }

                    Vector3 forward = this.transform.forward * rayLength;

                    //draw LineRenderer between pos0 and pos1
                    myLine.SetPosition(0, indexTip.transform.position);
                    myLine.SetPosition(1, forward);

                    // tint color on raycasthit, when tintMenu is active 
                    if (OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch))
                    {
                        myLine.enabled = true;

                        Vector3 forward1 = this.transform.forward * rayLength;

                        RaycastHit hit;
                        if (tintMode == true)
                        {
                            if (Physics.Raycast(indexTip.transform.position, forward1, out hit, layerMask))
                            {
                                var selection = hit.transform;
                                if (selection.CompareTag(selectableTag))
                                {
                                    var selectionRenderer = selection.GetComponent<Renderer>();
                                    if (selectionRenderer != null)
                                    {
                                        // reference and set this from menu interaction
                                        selectionRenderer.material = tintMaterial;
                                    }

                                    _selection = selection;
                                    lastTarget = _selection.gameObject;
                                    defaultMaterial = tintMaterial;
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
}
