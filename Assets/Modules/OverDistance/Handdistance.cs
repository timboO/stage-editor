﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handdistance : MonoBehaviour
{
    public GameObject rightHand;
    public GameObject leftHand;

    public float handDistance = 0f;
    public float scaleMult = 2f;

    public GameObject ScaleObject;


    // Update is called once per frame
    void Update()
    {
        if(ScaleObject != null)
        {
            handDistance = Vector3.Distance(rightHand.transform.position, leftHand.transform.position) * scaleMult;
            ScaleObject.transform.localScale = new Vector3(handDistance, handDistance, handDistance);

        }
    }
}
