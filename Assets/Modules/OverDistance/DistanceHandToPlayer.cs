﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceHandToPlayer : MonoBehaviour
{
    public GameObject centerEyeAnchor;
    public GameObject leftHand;

    private Vector3 forward;

    public float curDistance = 0f;
    public float distMult = 5f;
    public float lerpValue = 0f;

    public GameObject transformObject;
    public GameObject startPos, targetPos;
    public Vector3 lerpVector;



    // Update is called once per frame
    void Update()
    {
        if (transformObject != null)
        {
            //forward = centerEyeAnchor.transform.forward * 50;
            curDistance = Vector3.Distance(centerEyeAnchor.transform.position, leftHand.transform.position) * distMult;

            //lerpVector = Mathf.Lerp()
            //transformObject.transform.position += forward * curDistance * Time.deltaTime;

            //lerpValue = Mathf.Lerp(startPos.transform.position.z, targetPos.transform.position.z, curDistance);
            //transformObject.transform.position = new Vector3(transformObject.transform.position.x, transformObject.transform.position.y, lerpValue);

            lerpValue = Mathf.Lerp(startPos.transform.position.x, targetPos.transform.position.x, curDistance);
            transformObject.transform.position = new Vector3(lerpValue, transformObject.transform.position.y, transformObject.transform.position.z);

        }
    }
}
