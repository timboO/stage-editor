﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagManager : MonoBehaviour
{
    public string Selectable = "Selectable";
    public string notSelectable = "NotSelectable";
    public Transform instancesParent;
    public List<Transform> selectables = new List<Transform>();


    [ContextMenu ("GetAllSelectables")]
    public void GetAllSelectables(){

        selectables.Clear();
        selectables.AddRange(GetComponentsInChildren<Transform>());

        //remove parent transform itself
        selectables.Remove(this.transform);
    }

    public void AddSelectable(Transform newSelectable){
        selectables.Add(newSelectable);
    }

    public void RemoveSelectable(Transform destroySelectable){
        selectables.Remove(destroySelectable);
    }

    public void SetSelectableActive(Transform currentSelectable){

        foreach(var sel in selectables){

            if(sel != currentSelectable)
            {
                sel.gameObject.tag = notSelectable;
                sel.gameObject.layer = 2; // 2 = ignore Raycast layer
            }
                               
        }
    }

    public void ResetAllToSelectable(){
        Debug.Log("Reset");

        foreach(var sel in selectables)
        {
            sel.gameObject.tag = Selectable;
            sel.gameObject.layer = 0; // 0 = default
        }
    }
}
