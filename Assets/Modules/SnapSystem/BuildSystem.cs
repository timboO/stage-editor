﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSystem : MonoBehaviour
{

    public Camera playerCam;
    public LayerMask layer;

    private GameObject previewObject = null;
    private Preview previewScript = null;

    public float stickTolerance = 1.5f;

    public bool isBuilding = false;
    private bool pauseBuilding = false;


    private void Update()
    {

    }


    public void NewBuild(GameObject _go)
    {

    }


    private void CancelBuild()
    {

    }


    private void StopBuild()
    {

    }



    public void PauseBuild(bool _value)
    {

    }


    private void DoBuildRay()
    {

    }
}
