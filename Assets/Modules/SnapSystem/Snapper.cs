﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snapper : MonoBehaviour
{
    public bool isGerüstSnap;
    public bool isPlattformSnap;
    public string selectableTag = "Selectable";
    public string notSelectableTag = "NotSelectable";
    public DistanceInteraction DistanceInteraction;
    public bool thisIsGrabbed = false;
    public Collider myTrigger;

    private float gerüstSnapOffset = 2.5f;
    private float plattformSnapOffset = 1.3f;


    [ContextMenu ("GetDistanceInteractionGO")]
    void Awake()
    {
        DistanceInteraction = GameObject.FindObjectOfType<DistanceInteraction>();
    }


    void Update()
    {
        thisIsGrabbed = false;
        myTrigger.enabled = false;

        if (DistanceInteraction.distanceMove == true)
        {
            myTrigger.enabled = true;

            if (this.CompareTag(selectableTag))
                thisIsGrabbed = true;
        }            

        if(myTrigger != null)
        {
            if (thisIsGrabbed == true)
                myTrigger.enabled = false;

            // deactivate sphere collider of parent snapper gameobject
            if (this.transform.parent.GetComponent<Snapper>() == true)
            {
                this.transform.GetComponentInParent<SphereCollider>().enabled = false;
                myTrigger.enabled = false;
            }                              
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(notSelectableTag))
        {
            //if selected object is plattform, use plattform offset
            if (this.GetComponent<Snapper>().isPlattformSnap == true)
            {
                if (other.GetComponent<Snapper>().isGerüstSnap == true)
                {
                    /*other.transform.SetParent(this.transform);
                    other.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + gerüstSnapOffset, this.transform.position.z);
                    other.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, other.transform.rotation.eulerAngles.y, this.transform.rotation.eulerAngles.z);
                    */
                    //other.transform.SetParent(this.transform);
                    this.transform.SetParent(other.transform);
                    this.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + plattformSnapOffset, other.transform.position.z);
                    this.transform.rotation = Quaternion.Euler(other.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, other.transform.rotation.eulerAngles.z);

                }
            }

            //if selected object is gerüst, use gerüst-offset
            if (this.GetComponent<Snapper>().isGerüstSnap == true)
            {
                if (other.GetComponent<Snapper>().isGerüstSnap == true)
                {
                    /*other.transform.SetParent(this.transform);
                    other.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + gerüstSnapOffset, this.transform.position.z);
                    other.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, other.transform.rotation.eulerAngles.y, this.transform.rotation.eulerAngles.z);
                    */
                    //other.transform.SetParent(this.transform);
                    this.transform.SetParent(other.transform);
                    this.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + gerüstSnapOffset, other.transform.position.z);
                    this.transform.rotation = Quaternion.Euler(other.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, other.transform.rotation.eulerAngles.z);

                }
            } 








        }                  
    }   
}
