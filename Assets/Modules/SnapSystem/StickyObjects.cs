﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyObjects : MonoBehaviour
{
    [Header("References")]
    public LayerMask layerMask;
    public string groundTag = "GroundTag";
    public GameObject distanceMoveController;
    public int rayLength = 50;
    private Vector3 rayDir;
    public TagManager tagmanager;


    [Header("Obj To Ground")]
    public float distGroundToObject;
    public float groundThresholdWithScale;
    public float groundObjThreshold = 0.5f;
    public float rotThreshold = 0.25f;
    private float yOffset;
    public float offsetHelper = 0f;

    /*[Header("Obj To Wall)")]
    public float distWallToObject;
    public float wallObjThreshold = 0.7f;*/

    [Header("Obj State")]
    public bool isDistanceGrabbed;
    public bool isDistHighlight;
    public bool isGrabbed;
    public bool isColisionWithHand;
    public bool isGroundArea;


    void Start()
    {
        // Find the Dist move Controller in the scene for references
        distanceMoveController = GameObject.Find("DistanceMoveController");
        isGroundArea = false;

        tagmanager = GameObject.FindObjectOfType<TagManager>();
    }


    void Update()
    {
        if (this.transform.parent == null)
        {
            // making sure, that interactable gameobjects can never be unchilded to tagmanager
            this.transform.SetParent(tagmanager.transform);
        }

        // Check if object is currently interacted with
        isDistanceGrabbed = distanceMoveController.GetComponent<DistanceInteraction>().distanceMove;

        // scale + threshold = Gravity behaviour Threshold
        groundThresholdWithScale = this.transform.localScale.y * groundObjThreshold;

        // check, if Object is currently highlighted by distanceRayCast Interaction
        isDistHighlight = distanceMoveController.GetComponent<DistanceInteraction>().highlighted;
        if (isDistHighlight == true)
        {
            rayDir = -Vector3.up * rayLength;

            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, -Vector3.up * rayLength, out hit, layerMask))
            {
                var ground = hit.transform;
                if (ground.CompareTag(groundTag))
                {
                    // get distance between object and ground
                    distGroundToObject = Vector3.Distance(this.transform.position, hit.point);

                    // deciding if object is in ground area or not 
                    if (distGroundToObject - rotThreshold <= groundThresholdWithScale)
                        isGroundArea = true;
                    else
                        isGroundArea = false;
                     

                    // if distance is below threshold 
                    if (distGroundToObject <= groundThresholdWithScale)
                    {   
                        // reset X&Z rotation 
                        this.transform.rotation = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0);

                        // set Pos to where the raycast hit is
                        yOffset = (this.transform.localScale.y * 0.5f) + offsetHelper;
                        this.transform.position = new Vector3(hit.point.x, hit.point.y + yOffset, hit.point.z);

                        // Check if object is currently interacted with
                        isDistanceGrabbed = distanceMoveController.GetComponent<DistanceInteraction>().distanceMove;

                    }                    
                }
            }
        }
    }
}
