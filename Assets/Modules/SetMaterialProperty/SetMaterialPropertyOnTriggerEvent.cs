﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetMaterialPropertyOnTriggerEvent : MonoBehaviour
{

    MaterialPropertyBlock block;
    public Renderer myRenderer;

    [Header("Float")]
    public string FloatName;
    private float ActivateDodge;

    // Update is called once per frame
    void Update()
    {
        if (block == null) block = new MaterialPropertyBlock();
        if (!myRenderer) myRenderer = GetComponent<Renderer>();

        if (myRenderer) myRenderer.SetPropertyBlock(block);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        //Set Bool True
        ActivateDodge = 1;
        if (FloatName != "") block.SetFloat(FloatName, ActivateDodge);

    }

    private void OnTriggerExit(Collider other)
    {

        // Debug.Log(other.name + "Has Exit");
        // Set Bool False
        ActivateDodge = 0;
        if (FloatName != "") block.SetFloat(FloatName, ActivateDodge);

    }



}
