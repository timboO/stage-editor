﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetMaterialProperty : MonoBehaviour
{

    MaterialPropertyBlock block;
    public Renderer myRenderer;

    [Header("Vector4")]
    public string V4Name;
    public Vector4 V4Value;

    [Header("Vector2")]
    public string V2Name;
    public Vector2 V2Value;

    [Header("Float")]
    public string FloatName;
    public float FloatValue;

    // Update is called once per frame
    void Update()
    {

        if (block == null) block = new MaterialPropertyBlock();
        if (!myRenderer) myRenderer = GetComponent<Renderer>();

        if (FloatName != "") block.SetFloat(FloatName, FloatValue);

        if (V4Name != "") block.SetVector(V4Name, V4Value);

        if (V2Name != "") block.SetVector(V2Name, V2Value);

        if(myRenderer) myRenderer.SetPropertyBlock(block);
    }
}
