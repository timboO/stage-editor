﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetMaterialPropertyInArea : MonoBehaviour
{
#if UNITY_EDITOR
    [Header ("Experimental")]
    public bool updateInEditor = false;
    Vector3 lastPos, lastSize;

    void Update()
    {
        if (!Application.isPlaying && updateInEditor)
        { 
            if (lastPos != transform.position || lastSize != transform.lossyScale)
            {
                UpdateMaterialProperties();
                lastPos = transform.position;
                lastSize = transform.lossyScale;
                //Debug.Log(transform.name + " Bounds changed");
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = colorValue;
        //Gizmos.DrawWireCube(bounds.center, bounds.size);
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }
#endif

    void Start()
    {
        UpdateMaterialProperties();
    }

    List<Renderer> possibleRenderers;
    public List<Renderer> selectedRenderers = new List<Renderer>();
    List<Renderer> removableRenderers = new List<Renderer>();
    List<SetMaterialPropertyInArea> otherSelectors;
    Bounds bounds;

    [ContextMenu("Update Material Properties")]
    void UpdateMaterialProperties() {

        GetMaterialsInRange();

        ResetMaterialsOutOfRange();

        foreach (var sr in selectedRenderers)
            SetMaterialProperty(sr);
    }

    void GetMaterialsInRange()
    {
        possibleRenderers = new List<Renderer>(transform.parent.GetComponentsInChildren<Renderer>());

        bounds = new Bounds(transform.position, transform.lossyScale);

        foreach (var r in possibleRenderers)
        {
            if (bounds.Contains(r.bounds.center))
            {
                if (!selectedRenderers.Contains(r)) selectedRenderers.Add(r);
            }
            else
            {
                selectedRenderers.Remove(r);
            }
        }
    }

    void ResetMaterialsOutOfRange()
    {
        otherSelectors = new List<SetMaterialPropertyInArea>(transform.parent.GetComponentsInChildren<SetMaterialPropertyInArea>());
        otherSelectors.Remove(this);

        //check all selectedRenderers of other material overrides
        foreach (var oS in otherSelectors)
            foreach (var sR in oS.selectedRenderers) possibleRenderers.Remove(sR);
        //reset remaining mats to default
        foreach (var pR in possibleRenderers)
            pR.SetPropertyBlock(null);
    }

    MaterialPropertyBlock block;
    [Header("Material properties to overwrite")]
    public string colorName;
    public Color colorValue = Color.white;

    void SetMaterialProperty(Renderer myRenderer)
    {
        if (block == null) block = new MaterialPropertyBlock();
        if(colorName != "") block.SetColor(colorName, colorValue);
        if (myRenderer) myRenderer.SetPropertyBlock(block);
    }
}
