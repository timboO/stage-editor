﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetButtonProperty_TS : MonoBehaviour
{

    MaterialPropertyBlock block;
    public Renderer myRenderer;

    //private Collider buttonCollider;
    public bool buttonPressed = false;
    public string indexTag = "Index";
    public bool isExitBehaviour = false;

    [Header("Textures")]
    public string texName;
    public Texture idleTexture;
    public Texture pressedTexture;
    


     void Start()
    {
        if (block == null) block = new MaterialPropertyBlock();
        if (!myRenderer) myRenderer = GetComponent<Renderer>();

        if (texName != "") block.SetTexture(texName, idleTexture);

        //buttonCollider = this.gameObject.GetComponent<Collider>();

    }

    /*IEnumerator instOnce()
    {
        yield return new WaitForEndOfFrame();
        buttonPressed = false;
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(indexTag))
        {
            if (buttonPressed == true)
            {
                buttonPressed = false;
            }
            else
            {
                buttonPressed = true;
            }
        }         
    }


    private void OnTriggerExit(Collider other)
    {
        if (isExitBehaviour == true)
        {
            if (other.CompareTag(indexTag))
            {
                buttonPressed = false;
            }
        }        
    }

    // Update is called once per frame
    void Update()
    {
        if (block == null) block = new MaterialPropertyBlock();
        if (!myRenderer) myRenderer = GetComponent<Renderer>();

        if (buttonPressed == false)
        {
            block.SetTexture(texName, idleTexture);
        }


        if (buttonPressed == true)
        { 
            block.SetTexture(texName, pressedTexture);
        }

        if(myRenderer) myRenderer.SetPropertyBlock(block);
    }
}
