﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetGlobalMaterialProperty : MonoBehaviour
{
    [Header("Vector4")]
    public string paramsName;
    public Vector4 paramsValue;

    public string vec4Name;
    public Vector4 vec4Value;

    [Header("Scale")]
    public string scaleName;
    public float scaleValue;

    [Header("Vector3")]
    public string posPropertyName;
    public Vector3 posValue;

    [Header("Vector2")]
    public string uvPosName;
    public Vector2 uvPos;

    [Header("Tile/UV")]
    public string tileName;
    public float tile;
    private float tileMultiplier;

    [Header("Floats")]
    public string float1Name;
    public float float1Value;

    public string float2Name;
    public float float2Value;

    public string float3Name;
    public float float3Value;

    public string float4Name;
    public float float4Value;

    [Header("Color")]
    public string LightColorProperty;
    public Color LightColor;

    

    void Start()
    {

    }


    void Update()
    {
        // Helper Objects Position = Shader V3 Position
        posValue = transform.position;


        // Vector4
        if (paramsName != "")
            Shader.SetGlobalVector(paramsName, paramsValue);

        if (vec4Name != "")
            Shader.SetGlobalVector(vec4Name, vec4Value);

        // Vector 3, mostly Helper Position
        if (posPropertyName != "")
            Shader.SetGlobalVector(posPropertyName, posValue);

        // Tile
        if (tileName != "")
        {
            // 0.1 = 1/10
            tileMultiplier = 1 / tile;
            Shader.SetGlobalFloat(tileName, tile);
        }
        
        // UV Space
        if (uvPosName != "")
        {
            uvPos = transform.position / tileMultiplier;
            Shader.SetGlobalVector(uvPosName, uvPos);
        }

        // Floats
        if (float1Name != "")
            Shader.SetGlobalFloat(float1Name, float1Value);

        if (float2Name != "")
            Shader.SetGlobalFloat(float2Name, float2Value);

        if (float3Name != "")
            Shader.SetGlobalFloat(float3Name, float3Value);

        if (float4Name != "")
            Shader.SetGlobalFloat(float4Name, float4Value);

        // Scale Float
        if (scaleName != "")
        {
            Shader.SetGlobalFloat(scaleName, scaleValue);

            // Helper Objects Scale = Shaders Mask Scale
            // Possibly moving this out of update into Start
            transform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);

        }



        // Color
        if (LightColorProperty != "")
            Shader.SetGlobalColor(LightColorProperty, LightColor);

    }
}
