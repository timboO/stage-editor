﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObject_Dynamic : MonoBehaviour
{

    public bool scaleUniform, scaleX, scaleY, scaleZ = false;
    public GameObject scaleObject;

    private Vector3 objectScale;

    public float uniformScale = 1;
    public float sizeX, sizeY, sizeZ = 1;

    // Start is called before the first frame update
    void Start()
    {

        scaleObject = this.gameObject;
        objectScale = scaleObject.transform.localScale;

    }

    // Update is called once per frame
    void Update()
    {
        uniformScale = Mathf.Clamp(uniformScale,0,100);

        // UI Button set this
        if (scaleUniform == true)
        {
            objectScale = new Vector3(uniformScale, uniformScale, uniformScale);
            scaleObject.transform.localScale = objectScale;
            
        }

        // UI Button set this
        if (scaleX == true | scaleY == true | scaleZ == true)
        {
            objectScale = new Vector3(sizeX, sizeY, sizeZ);
            scaleObject.transform.localScale = objectScale;
        }


    }
}
