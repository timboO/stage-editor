﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObject_Units : MonoBehaviour
{

    public bool scaleUniform, scaleX, scaleY, scaleZ;
    public GameObject scaleObject;

    public Vector3 objectScale;
    public float scaleUnits;

    // Start is called before the first frame update
    void Start()
    {
        scaleUniform = true;
        scaleX = false;
        scaleY = false;
        scaleZ = false;

        scaleObject = this.gameObject;
        objectScale = scaleObject.transform.localScale;
        

    }

    // Update is called once per frame
    void Update()
    {
        
        if (scaleUniform == true)
        {
            if (Input.GetMouseButtonDown(1))
            {
                scaleUnits = Mathf.Clamp(scaleUnits, 0, 100);
                objectScale = objectScale + new Vector3(scaleUnits, scaleUnits, scaleUnits);
                scaleObject.transform.localScale = objectScale;
            }
        }


        if (scaleX == true)
        {

        }


        if (scaleY == true)
        {


        }


        if (scaleZ == true)
        {


        }


    }
}
