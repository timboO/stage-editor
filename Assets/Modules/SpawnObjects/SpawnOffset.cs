﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOffset : MonoBehaviour
{
    [Header("Interaction States")]
    public bool erschaffenActive;
    public bool menuActive;

    [Header("References")]
    public GameObject uiInteraction;
    public GameObject centerEyeAnchor;
    private GameObject myGameobject;
    public GameObject myTarget;
    public ParticleSystem spawnParticles1, spawnParticles2;
    public bool isPlaying = false;
    public GameObject Player;

    [Header("Adjustments")]
    public float yPos;
    private Vector3 PlayerPos;
    public float hitpointOffset = 0.03f;

    [Header("Raycast Interaction")]
    public float rayLength = 100f;
    public LayerMask layerMask;

    


    void Start()
    {
        if(myGameobject == null)
            myGameobject = this.gameObject;

        erschaffenActive = false;
        isPlaying = false;
    }


    void Update()
    {
        menuActive = uiInteraction.GetComponent<MenuActive>().menuActive;
        erschaffenActive = uiInteraction.GetComponent<MenuActive>().ersActive;

        // while menuClosed, this GO has a fixed PosY
        PlayerPos = Player.transform.position;
        myGameobject.transform.position = new Vector3(myGameobject.transform.position.x, PlayerPos.y + yPos, myGameobject.transform.position.z);

        // while menuCLosed, target is not visible
        myTarget.GetComponent<Renderer>().enabled = false;

        isPlaying = false;
        var emission = spawnParticles1.emission;
        emission.enabled = isPlaying;

        var emission2 = spawnParticles2.emission;
        emission2.enabled = isPlaying;

        // Raycast interaction only when menu + erschaffen state  isActive 
        if ((erschaffenActive == true) && (menuActive == true))
        {
            isPlaying = true;
            emission.enabled = isPlaying;
            emission2.enabled = isPlaying;

            RaycastHit hit;
            if (Physics.Raycast(myGameobject.transform.position, -myGameobject.transform.up, out hit, layerMask))
            {
                // Set the Position of the SpawnPos to where the Rycast hit the collider
                myTarget.transform.position = new Vector3(hit.point.x, hit.point.y + hitpointOffset, hit.point.z);
                myGameobject.transform.localRotation = Quaternion.Euler(-centerEyeAnchor.transform.rotation.eulerAngles.x, 0, -centerEyeAnchor.transform.rotation.eulerAngles.z);

                // while menuOpened, target is visible
                myTarget.GetComponent<Renderer>().enabled = true;                
            }                   
        }
    }   
}
