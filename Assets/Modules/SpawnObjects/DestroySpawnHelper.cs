﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySpawnHelper : MonoBehaviour
{
    // Start is called before the first frame update
    TagManager tagManager;

    void Awake()
    {
        tagManager = GameObject.FindObjectOfType<TagManager>();

        var children = GetComponentsInChildren<Transform>();
        this.transform.DetachChildren();

        foreach(var child in children)
            if(child != null && child != this.transform) 
            {
                child.SetParent(tagManager.transform);
                tagManager.AddSelectable(child);
            }      

        //tagManager.RemoveSelectable(this.transform);
        Destroy(this.gameObject);      
    }

}
