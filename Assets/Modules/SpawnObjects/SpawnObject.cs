﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{

    public Transform spawnPos;

    public bool chooseObj1, chooseObj2, chooseObj3, chooseObj4;

    public GameObject Stuhl, Bank, Gerüst, Treppe;

    


    // Start is called before the first frame update
    void Start()
    {
        chooseObj1 = false;
        chooseObj2 = false;
        chooseObj3 = false;
        chooseObj4 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (chooseObj1 == true)
        {
            Instantiate(Stuhl, spawnPos.position, spawnPos.rotation);
        }
       

        if (chooseObj2 == true)
        {
                Instantiate(Bank, spawnPos.position, spawnPos.rotation);
        }


        if (chooseObj3 == true)
        {
                Instantiate(Gerüst, spawnPos.position, spawnPos.rotation);
        }


        if (chooseObj4 == true)
        {
                Instantiate(Treppe, spawnPos.position, spawnPos.rotation);
        }

    }
}
