﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPhysicsAfterInteraction : MonoBehaviour
{
    
    private Rigidbody rb;

    [Header("Debug")]
    public bool interacted = false;

    [Header("Adjustments")]
    public float waitDur = 0.5f;

    // when object is interacted with, use gravity for waitDur
    private IEnumerator objectWasInteracted()
    {
        // deactivate kinematic, activate Gravity
        rb.useGravity = true;
        rb.isKinematic = false;

        // duration for the behaviour be active
        yield return new WaitForSeconds(waitDur);

        //activate kinematic, deactivate gravity
        rb.useGravity = false;
        rb.isKinematic = true;

        // making sure ienum doesnt loop
        interacted = false;

    }


    // Start is called before the first frame update
    void Start()
    {
        if (!rb) rb = this.GetComponent<Rigidbody>();  
    }


    // on object spawn - have gravity behaviour active
    public void Awake()
    {
        objectWasInteracted();
    }


    // Update is called once per frame
    void Update()
    {
        // gets called after DistanceInteraction
        if (interacted == true)
        {
            objectWasInteracted();
        }
    }
}
