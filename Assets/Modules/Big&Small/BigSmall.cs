﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigSmall : MonoBehaviour
{
    [Header("Debug States")]
    public bool playerIsBig = false;
    public float bigScale = 10f;
    public float smallScale = 1f;
    public bool lefthandPressed = false;
    public bool righthandPressed = false;
    public float changeRate = 1.5f; // can change after x sec
    private float nextChange = 0f;

    [Header("References")]
    public GameObject player;
    public GameObject smallTargetPos;
    public GameObject bigTargetPos;
    public DistanceInteraction distanceInteraction;

    [Header("Object Activations")]
    public GameObject Dach1;
    public GameObject Dach2;
    public GameObject Blende;
    //public GameObject DirLights;
    //public GameObject Spotlights;



    void Start()
    {
        if (Dach1 && Dach2 && Blende != null)
        {
            Dach1.SetActive(true);
            Dach2.SetActive(true);
            Blende.SetActive(true);
            //Spotlights.SetActive(true);
            //DirLights.SetActive(false);
        }

        playerIsBig = false;

        distanceInteraction = GameObject.FindObjectOfType<DistanceInteraction>();
    }


    void Update()
    {
        if(distanceInteraction.selectRay == false)
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch))
                lefthandPressed = true;
            if (OVRInput.GetUp(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch))
                lefthandPressed = false;

            if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.RTouch))
                righthandPressed = true;
            if (OVRInput.GetUp(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.RTouch))
                righthandPressed = false;


            if (lefthandPressed && righthandPressed == true)
            {
                if ((playerIsBig == false) && (Time.time > nextChange))
                {
                    nextChange = Time.time + changeRate;
                    playerIsBig = true;
                    this.SetPlayerBig();
                }

                if ((playerIsBig == true) && (Time.time > nextChange))
                {
                    nextChange = Time.time + changeRate;
                    playerIsBig = false;
                    this.SetPlayerSmall();
                }
            }
        }             
    }


    public void SetPlayerBig()
    {
        player.transform.position = bigTargetPos.transform.localPosition;
        player.transform.rotation = bigTargetPos.transform.localRotation;
        player.transform.localScale = new Vector3(bigScale, bigScale, bigScale);

        if ( Dach1 && Dach2 && Blende != null)
        {
            Dach1.SetActive(false);
            Dach2.SetActive(false);
            Blende.SetActive(false);
            //Spotlights.SetActive(false);
            //DirLights.SetActive(true);
        }

    }

    public void SetPlayerSmall()
    {
        player.transform.position = smallTargetPos.transform.localPosition;
        player.transform.rotation = smallTargetPos.transform.localRotation;
        player.transform.localScale = new Vector3(smallScale, smallScale, smallScale);

        if (Dach1 && Dach2 && Blende != null)
        {
            Dach1.SetActive(true);
            Dach2.SetActive(true);
            Blende.SetActive(true);
            //Spotlights.SetActive(true);
            //DirLights.SetActive(false); 
        }
    }
}
