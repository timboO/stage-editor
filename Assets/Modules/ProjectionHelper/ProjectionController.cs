﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ProjectionController : MonoBehaviour
{

    MaterialPropertyBlock block;
    public Renderer myRenderer;

    [Header("Base Settings")]
    public string string1Name;
    public float GetYrot;
    public float minAngle = 0f;
    public float maxAngle = 90f;

    [Header("Helper Object")]
    public GameObject Helper;
    public string string2Name;
    public Vector3 offset;

    [Header("Projection Texture")]
    public string string3Name;
    public Texture myTex;
    public string string4Name;
    public float tiling;
    

    void Start()
    {
        //block = new MaterialPropertyBlock();
    }


    void Update()
    {

        if (block == null) block = new MaterialPropertyBlock();
        if (!myRenderer) myRenderer = GetComponent<Renderer>();

        //Link this value to the materials Rotation value
        GetYrot = this.transform.localRotation.eulerAngles.y;
        if (string1Name != "")block.SetFloat(string1Name, -GetYrot);

        // Links the Helper Object to the Vector in the shader as local override
        offset = Helper.transform.position;
        if (string2Name != "")block.SetVector(string2Name, -offset);

        if (string3Name != "")block.SetTexture(string3Name, myTex);

        tiling = 1 / Helper.transform.localScale.x;
        if (string4Name != "") block.SetFloat(string4Name, tiling);


        if (myRenderer) myRenderer.SetPropertyBlock(block);


    }
}
