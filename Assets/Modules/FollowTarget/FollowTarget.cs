﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{

    public Transform target;
    private Transform gameObj;
    public float yOffset;
    public float curOffset;

    // Start is called before the first frame update
    void Start()
    {
        gameObj = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        gameObj.transform.rotation = target.transform.rotation;
        gameObj.transform.localScale = transform.localScale;
        gameObj.transform.position = new Vector3(target.transform.position.x, curOffset, target.transform.position.z);
    }

    public void SetOffset()
    {
        curOffset = target.transform.position.y + yOffset;
    }

}

