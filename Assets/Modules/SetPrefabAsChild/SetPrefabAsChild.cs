﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPrefabAsChild : MonoBehaviour
{
    public GameObject prefabAsChild;


    [ContextMenu("Set Prefab as Child")]
    public void SetPreabAsChild()
    {
        prefabAsChild.transform.SetParent(this.gameObject.transform);
        //prefabAsChild.transform.rotation = this.transform.rotation;


    }
}
