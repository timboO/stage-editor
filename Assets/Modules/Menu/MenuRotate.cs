﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRotate : MonoBehaviour
{
    [Header("References")]
    public Animator uiAnimator;
    public string myAnimatorBoolName = "NextTrue";
    public string indexTag = "Index";
    


    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == indexTag)
        {
            uiAnimator.SetBool(myAnimatorBoolName, true);
 
        }

    }


    public void OnTriggerExit(Collider other)
    {
        if(other.tag == indexTag)
        {
            uiAnimator.SetBool(myAnimatorBoolName, false);
        }
        
    }

}
