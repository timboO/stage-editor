﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberCounter : MonoBehaviour
{
    public int counter = 1;
    public Material counterMat1, counterMat2, counterMat3, counterMat4, counterMat5;
    public GameObject curCounter;
    public GameObject higherNr, lowerNr;
    public float counterRate = 0.4f;
    private float counterInterval = 0f;
    public MenuActive menuActive;


    void Start()
    {
        counter = 1;
        curCounter.GetComponent<Renderer>().material = counterMat1;
        menuActive = GameObject.FindObjectOfType<MenuActive>();
    }



    void Update()
    {
            if ((higherNr.GetComponent<SetButtonProperty_TS>().buttonPressed == true) && (Time.time > counterInterval))
            {
                counterInterval = Time.time + counterRate;
                counter = counter + 1;
            }

            if ((lowerNr.GetComponent<SetButtonProperty_TS>().buttonPressed == true) && (Time.time > counterInterval))
            {
                counterInterval = Time.time + counterRate;
                counter = counter - 1;
            }

            if(counter > 5)
                counter = 5;

            if(counter < 1)
                counter = 1;



            if (counter == 1)
            {
                curCounter.GetComponent<Renderer>().material = counterMat1;
            }

            if (counter == 2)
            {
                curCounter.GetComponent<Renderer>().material = counterMat2;
            }

            if (counter == 3)
            {
                curCounter.GetComponent<Renderer>().material = counterMat3;
            }

            if (counter == 4)
            {
                curCounter.GetComponent<Renderer>().material = counterMat4;
            }

            if (counter == 5)
            {
                curCounter.GetComponent<Renderer>().material = counterMat5;
            }
                  
    }
}
