﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuActive : MonoBehaviour
{
    [Header("Links (dont change those)")]
    public string hauptmenuState;
    public string erschaffenState;
    public string aussehenState;

    [Header("Current States (Debug)")]
    public string currentState;
    public bool menuActive;
    public bool hmActive, ersActive, ausActive;

    [Header("GO Activates")]
    public GameObject uiMenu;
    private Animator myUIAnimator;
    public AnimatorClipInfo[] myClipInfo;
    public GameObject hauptmenu, erschaffen, aussehen;
    public GameObject objContainer;
    public GameObject platzhalter;
    public Collider indexCollider;

    [Header("Spawn")]
    public Transform instancesParent;
    public TagManager tagManager;
    public GameObject spawnTarget;
    public float spawnRate = 2f; // each 2 seconds spawn 1 object
    private float nextSpawn = 0.0f;
    public GameObject centerEyeAnchor;
    private Quaternion spawnRot;
    

    [Header("Buttons")]
    public GameObject selObjekte;
    public bool objekteTrue;
    public GameObject selPlatzhalter;
    public bool platzhalterTrue;

    public GameObject selChair;
    private bool chairTrue;
    public GameObject selBench;
    private bool benchTrue;
    public GameObject selStairs;
    private bool stairsTrue;
    public GameObject selFrame;
    private bool frameTrue;
    public GameObject selPlattform;
    private bool plattformTrue;
    public GameObject selTable;
    private bool tableTrue;
    public GameObject selKlTreppe;
    private bool klTreppeTrue;
    public GameObject selGeländer;
    private bool geländerTrue;
    public GameObject selBestätigen;
    private bool bestätigenTrue;

    public GameObject selCube;
    private bool cubeTrue;
    public GameObject selCyl;
    private bool cylTrue;
    public GameObject selStange;
    private bool stangeTrue;
    public GameObject selSphere;
    private bool sphereTrue;
    public GameObject selScreen;
    private bool screenTrue;
    public GameObject selCharStand;
    private bool charStandTrue;
    public GameObject selCharSit;
    private bool charSitTrue;

    public int curCounter = 1;
    public GameObject curCounterGO;
    //public GameObject mod1x;
    //public bool mod1True;
    //public GameObject mod5x;
    //public bool mod5True;
    //public GameObject mod10x;
    //public bool mod10True;
    //public GameObject modLine;
    //public bool modLineTrue;
    //public GameObject modCurved;
    //public bool modCurvedTrue;

    [Header("Spawn Objects")]
    public GameObject Stuhl1x;
    public GameObject Stuhl2x;
    public GameObject Stuhl3x;
    public GameObject Stuhl4x;
    public GameObject Stuhl5x;
    public GameObject Bank1x;
    public GameObject Bank2x;
    public GameObject Bank3x;
    public GameObject Bank4x;
    public GameObject Bank5x;
    public GameObject Stairs1x;
    public GameObject Stairs2x;
    public GameObject Stairs3x;
    public GameObject Stairs4x;
    public GameObject Stairs5x;
    public GameObject Frame1x;
    public GameObject Frame2x;
    public GameObject Frame3x;
    public GameObject Frame4x;
    public GameObject Frame5x;
    public GameObject Table1x;
    public GameObject Table2x;
    public GameObject Table3x;
    public GameObject Table4x;
    public GameObject Table5x;
    public GameObject Plattform1x;
    public GameObject Plattform2x;
    public GameObject Plattform3x;
    public GameObject Plattform4x;
    public GameObject Plattform5x;
    public GameObject klTreppe1x;
    public GameObject klTreppe2x;
    public GameObject klTreppe3x;
    public GameObject klTreppe4x;
    public GameObject klTreppe5x;
    public GameObject Geländer1x;
    public GameObject Geländer2x;
    public GameObject Geländer3x;
    public GameObject Geländer4x;
    public GameObject Geländer5x;


    [Header("Spawn Placeholders")]
    public GameObject Cube1x;
    public GameObject Cube2x;
    public GameObject Cube3x;
    public GameObject Cube4x;
    public GameObject Cube5x;
    public GameObject Cyl1x, Cyl2x, Cyl3x, Cyl4x, Cyl5x;
    public GameObject Stange1x, Stange2x, Stange3x, Stange4x, Stange5x;
    public GameObject Sphere1x, Sphere2x, Sphere3x, Sphere4x, Sphere5x;
    public GameObject Screen1x, Screen2x, Screen3x, Screen4x, Screen5x;
    public GameObject chartStand1x, chartStand2x, chartStand3x, chartStand4x, chartStand5x;
    public GameObject charSit1x, charSit2x, charSit3x, charSit4x, charSit5x;

    [Header("Raster")]
    public GameObject rasterButton;
    public bool rasterTrue;
    public GameObject rasterController;
    public float rasterValue;
    public GameObject Hilfslinien;

    [Header("Tint Buttons")]
    public GameObject DistanceInteraction;
    public GameObject selTint1;
    public Material b1Tint;
    private bool tint1True;
    public GameObject selTint2;
    public Material b2Tint;
    private bool tint2True;
    public GameObject selTint3;
    public Material b3Tint;
    private bool tint3True;
    public GameObject selTint4;
    public Material b4Tint;
    private bool tint4True;
    public GameObject selTint5;
    public Material b5Tint;
    private bool tint5True;
    public GameObject selTint6;
    public Material b6Tint;
    private bool tint6True;
    public GameObject selTint7;
    public Material b7Tint;
    private bool tint7True;

    

    // Start is called before the first frame update
    void Start()
    {
        indexCollider.enabled = false;

        myUIAnimator = uiMenu.GetComponent<Animator>();
 
        uiMenu.SetActive(true);
        hauptmenu.SetActive(false);
        erschaffen.SetActive(false);
        aussehen.SetActive(false);

        menuActive = false;
        hmActive = false;
        ersActive = false;
        ausActive = false;
    }


    // Update is called once per frame
    void Update()
    {
        // get the current number of objects to be spawned
        curCounter = curCounterGO.GetComponent<NumberCounter>().counter;

        spawnRot = Quaternion.Euler(0, centerEyeAnchor.transform.rotation.eulerAngles.y - 180, 0);
        
        myClipInfo = myUIAnimator.GetCurrentAnimatorClipInfo(0);
        currentState = myClipInfo[0].clip.name;

        // in which state is the animator
        if (string.Compare(currentState, hauptmenuState) == 0)
        {
            if(menuActive == true)
            {
                hmActive = true;
                ersActive = false;
                ausActive = false;

                // only active while menu active
                // while grabbing, its scale is somehow getting bigger
                indexCollider.enabled = true;
            }
            else
            {
                hmActive = false;
                //indexCollider.enabled = false;
            }   
        }

        //activate menu
        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.LTouch))
        {
            myClipInfo = myUIAnimator.GetCurrentAnimatorClipInfo(0);
            currentState = myClipInfo[0].clip.name;

            hauptmenu.SetActive(true);
            erschaffen.SetActive(true);
            aussehen.SetActive(true);

            // check if menu is already active
            // if so, deactivate it again
            if (menuActive == true)
            {
                menuActive = false;
                hmActive = false;

                hauptmenu.SetActive(false);
                erschaffen.SetActive(false);
                aussehen.SetActive(false);
            }
            else
            {
                menuActive = true;
                hmActive = true;

                hauptmenu.SetActive(true);
                erschaffen.SetActive(true);
                aussehen.SetActive(true);

            }

        }


        if (string.Compare(currentState, erschaffenState) == 0)
        {
            hmActive = false;
            ersActive = true;
            ausActive = false;

            indexCollider.enabled = true;

            // bool states from header buttons
            objekteTrue = selObjekte.GetComponent<SetButtonProperty_TS>().buttonPressed;
            platzhalterTrue = selPlatzhalter.GetComponent<SetButtonProperty_TS>().buttonPressed;

            // objekte bool checks from buttons
            chairTrue = selChair.GetComponent<SetButtonProperty_TS>().buttonPressed;
            benchTrue = selBench.GetComponent<SetButtonProperty_TS>().buttonPressed;
            stairsTrue = selStairs.GetComponent<SetButtonProperty_TS>().buttonPressed;
            frameTrue = selFrame.GetComponent<SetButtonProperty_TS>().buttonPressed;
            klTreppeTrue = selKlTreppe.GetComponent<SetButtonProperty_TS>().buttonPressed;
            geländerTrue = selGeländer.GetComponent<SetButtonProperty_TS>().buttonPressed;
            plattformTrue = selPlattform.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tableTrue = selTable.GetComponent<SetButtonProperty_TS>().buttonPressed;

            bestätigenTrue = selBestätigen.GetComponent<SetButtonProperty_TS>().buttonPressed;

            // platzhalter bool checks from buttons
            cubeTrue = selCube.GetComponent<SetButtonProperty_TS>().buttonPressed;
            cylTrue = selCyl.GetComponent<SetButtonProperty_TS>().buttonPressed;
            sphereTrue = selSphere.GetComponent<SetButtonProperty_TS>().buttonPressed;
            screenTrue = selScreen.GetComponent<SetButtonProperty_TS>().buttonPressed;
            charStandTrue = selCharStand.GetComponent<SetButtonProperty_TS>().buttonPressed;
            charSitTrue = selCharSit.GetComponent<SetButtonProperty_TS>().buttonPressed;
            stangeTrue = selStange.GetComponent<SetButtonProperty_TS>().buttonPressed;



            if (platzhalterTrue == false)
            {
                //deactivate selection
                platzhalter.SetActive(false);
            }

            // Platzhalter ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(platzhalterTrue == true)
            {
                //activate selection
                platzhalter.SetActive(true);

                // spawn cube1x
                if (cubeTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Cube1x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }                                     
                }

                if (cubeTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Cube2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (cubeTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            //GameObject newSelectable = Instantiate(Cube1xLine, spawnPos.transform.position, spawnRot, instancesParent);
                            //tagManager.AddSelectable(newSelectable.transform);
                            InstantiateSelectable(Cube3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (cubeTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            //GameObject newSelectable = Instantiate(Cube1xLine, spawnPos.transform.position, spawnRot, instancesParent);
                            //tagManager.AddSelectable(newSelectable.transform);
                            InstantiateSelectable(Cube4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (cubeTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            //GameObject newSelectable = Instantiate(Cube1xLine, spawnPos.transform.position, spawnRot, instancesParent);
                            //tagManager.AddSelectable(newSelectable.transform);
                            InstantiateSelectable(Cube5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                // spawn cyl1x
                if (cylTrue == true)
                {
                    if(curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Cyl1x, spawnTarget.transform.position, spawnRot, instancesParent);                               
                            }

                    }  
                }

                if (cylTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Cyl2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (cylTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Cyl3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (cylTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Cyl4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (cylTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Cyl5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                // spawn stange
                if (stangeTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stange1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (stangeTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stange2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (stangeTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stange3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (stangeTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stange4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (stangeTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stange5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }


                // spawn spehre1x
                if (sphereTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Sphere1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (sphereTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Sphere2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (sphereTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Sphere3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (sphereTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Sphere4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (sphereTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Sphere5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                // spawn screen1x
                if (screenTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Screen1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (screenTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Screen2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (screenTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Screen3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (screenTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Screen4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (screenTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Screen5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }


                // spawn standingChar                
                if (charStandTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(chartStand1x, spawnTarget.transform.position, spawnRot, instancesParent);  
                            }
                    }    
                }

                if (charStandTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(chartStand2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (charStandTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(chartStand3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (charStandTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(chartStand4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (charStandTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(chartStand5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }


                // spawn sitChar                
                if (charSitTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(charSit1x, spawnTarget.transform.position, spawnRot, instancesParent);
                               
                            }
                    }    
                }

                if (charSitTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(charSit2x, spawnTarget.transform.position, spawnRot, instancesParent);

                        }
                    }
                }

                if (charSitTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(charSit3x, spawnTarget.transform.position, spawnRot, instancesParent);

                        }
                    }
                }

                if (charSitTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(charSit4x, spawnTarget.transform.position, spawnRot, instancesParent);

                        }
                    }
                }

                if (charSitTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(charSit5x, spawnTarget.transform.position, spawnRot, instancesParent);

                        }
                    }
                }
            }

            if(objekteTrue == false)
            {
                //deactivate selection
                objContainer.SetActive(false);
            }
            // Objekte ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(objekteTrue == true)
            {
                //activate selection
                objContainer.SetActive(true);

                // Stuhl
                // spawn chair1xLine
                if (chairTrue == true)
                {
                    if (curCounter == 1)
                    {                        
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stuhl1x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }     
                }

                // spawn chair2x
                if (chairTrue == true)
                {
                    if (curCounter == 2)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stuhl2x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }    
                }

                // spawn chair3x
                if (chairTrue == true)
                {
                    if (curCounter == 3)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stuhl3x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }
                }

                // spawn chair4x
                if (chairTrue == true)
                {
                    if (curCounter == 4)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stuhl4x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }                        
                }

                // spawn chair5x
                if (chairTrue == true)
                {   
                    if (curCounter == 5)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stuhl5x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                        
                    }                        
                }


                // Bank
                // spawn bench1xLine
                if (benchTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Bank1x, spawnTarget.transform.position, spawnRot, instancesParent);
                            } 
                    }                        
                }

                if (benchTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Bank2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (benchTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Bank3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (benchTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Bank4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                // spawn bench5x
                if (benchTrue == true)
                {
                    if (curCounter == 5)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Bank5x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }
                }
             
                // Treppe
                // spawn treppe1xLine
                if (stairsTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Stairs1x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }
                    }                       
                }

                if (stairsTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stairs2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (stairsTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stairs3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (stairsTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stairs4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                if (stairsTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Stairs5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }
                    }
                }

                // Gerüst
                // spawn Gerüst1xLine
                if (frameTrue == true)
                {
                    if (curCounter == 1)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Frame1x, spawnTarget.transform.position, spawnRot, instancesParent);
                            }

                    }                        
                }

                if (frameTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Frame2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (frameTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Frame3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (frameTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Frame4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                // spawn gerüst5xline
                if (frameTrue == true)
                {
                    if (curCounter == 5)
                    {
                            if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                            {
                                nextSpawn = Time.time + spawnRate;
                                InstantiateSelectable(Frame5x, spawnTarget.transform.position, spawnRot, instancesParent);                        
                            }
                    }                        
                }

                //spawn Plattform
                if (plattformTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Plattform1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (plattformTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Plattform2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (plattformTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Plattform3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (plattformTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Plattform4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (plattformTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Plattform5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                // spawn table
                if (tableTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Table1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (tableTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Table2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (tableTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Table3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (tableTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Table4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (tableTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Table5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }


                // spawn barchair
                if (klTreppeTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(klTreppe1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (klTreppeTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(klTreppe2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (klTreppeTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(klTreppe3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (klTreppeTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(klTreppe4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (klTreppeTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(klTreppe5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                // spawn bar table
                if (geländerTrue == true)
                {
                    if (curCounter == 1)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Geländer1x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (geländerTrue == true)
                {
                    if (curCounter == 2)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Geländer2x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (geländerTrue == true)
                {
                    if (curCounter == 3)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Geländer3x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (geländerTrue == true)
                {
                    if (curCounter == 4)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Geländer4x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

                if (geländerTrue == true)
                {
                    if (curCounter == 5)
                    {
                        if ((bestätigenTrue == true) && (Time.time > nextSpawn))
                        {
                            nextSpawn = Time.time + spawnRate;
                            InstantiateSelectable(Geländer5x, spawnTarget.transform.position, spawnRot, instancesParent);
                        }

                    }
                }

            }            
        }
        

        if (string.Compare(currentState, aussehenState) == 0)
        {
            hmActive = false;
            ersActive = false;
            ausActive = true;

            indexCollider.enabled = true;

            tint1True = selTint1.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint2True = selTint2.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint3True = selTint3.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint4True = selTint4.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint5True = selTint5.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint6True = selTint6.GetComponent<SetButtonProperty_TS>().buttonPressed;
            tint7True = selTint7.GetComponent<SetButtonProperty_TS>().buttonPressed;

            rasterTrue = rasterButton.GetComponent<SetButtonProperty_TS>().buttonPressed;


            // tint color whenever there is exactly 1 color picked
            if ((tint1True == true) && (tint2True == false) && (tint3True == false) && (tint4True == false) && (tint5True == false) && (tint6True == false) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b1Tint;
            }

            if ((tint1True == false) && (tint2True == true) && (tint3True == false) && (tint4True == false) && (tint5True == false) && (tint6True == false) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b2Tint;
            }

            if ((tint1True == false) && (tint2True == false) && (tint3True == true) && (tint4True == false) && (tint5True == false) && (tint6True == false) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b3Tint;
            }

            if ((tint1True == false) && (tint2True == false) && (tint3True == false) && (tint4True == true) && (tint5True == false) && (tint6True == false) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b4Tint;
            }

            if ((tint1True == false) && (tint2True == false) && (tint3True == false) && (tint4True == false) && (tint5True == true) && (tint6True == false) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b5Tint;
            }

            if ((tint1True == false) && (tint2True == false) && (tint3True == false) && (tint4True == false) && (tint5True == false) && (tint6True == true) && (tint7True == false))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b6Tint;
            }

            if ((tint1True == false) && (tint2True == false) && (tint3True == false) && (tint4True == false) && (tint5True == false) && (tint6True == false) && (tint7True == true))
            {
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b7Tint;
            }

            //when no color is selected
            if ((tint1True == false) && (tint2True == false) && (tint3True == false) && (tint4True == false) && (tint5True == false) && (tint6True == false) && (tint7True == false))
            {
                //DistanceInteraction.GetComponent<DistanceInteraction>().tintMode = false;
                DistanceInteraction.GetComponent<DistanceInteraction>().tintMaterial = b7Tint; // white mat
            }

            // Raster on/off behaviour
            if (rasterTrue == true)
            {
                rasterValue = 1;
                rasterController.GetComponent<SetGlobalMaterialProperty>().float1Value = rasterValue;
                if(Hilfslinien != null)
                    Hilfslinien.SetActive(true);
            }
            else
            {
                rasterValue = 0;
                rasterController.GetComponent<SetGlobalMaterialProperty>().float1Value = rasterValue;
                if (Hilfslinien != null)
                    Hilfslinien.SetActive(false);
            }

        }
    }

    // GameObject newSelectable = Instantiate(Cube1xLine, spawnPos.transform.position, spawnRot, instancesParent);
    // tagManager.AddSelectable(newSelectable.transform);

    public void InstantiateSelectable(GameObject newObject, Vector3 position, Quaternion rotation, Transform parent){

        GameObject newSelectable = Instantiate(newObject, position, rotation, parent);
        //tagManager.AddSelectable(newSelectable.transform);
    }

}
