﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActivatesGO : MonoBehaviour
{
    public GameObject targetGO;

    // Start is called before the first frame update
    void Start()
    {
        targetGO.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(this.GetComponent<SetButtonProperty_TS>().buttonPressed == true)
        {
            targetGO.SetActive(true);
        }else{
            targetGO.SetActive(false);
        }
    }
}
